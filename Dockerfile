FROM nginx:stable-alpine

ENV PORT 80
EXPOSE 80

COPY ./build /usr/share/nginx/html/
ENTRYPOINT ["nginx","-g","daemon off;"]
